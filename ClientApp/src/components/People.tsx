import React, {FunctionComponent} from 'react';
import {Table} from "reactstrap";

type Person = {
    id: number;
    name: string;
    gender: string;
    dateOfBirth: string;
    hairColor: string;
    height: number;
    weight: number;
}

type PeopleState = {
    people: Person[]
}

export class People extends React.PureComponent<{}, PeopleState> {
    state = {
        people: [
            {id: 1, name: "Luke", gender: "male", dateOfBirth: "01.12.1900", hairColor: "blonde", height: 1.80, weight: 95.6},
            {id: 2, name: "Mary", gender: "female", dateOfBirth: "01.12.1850", hairColor: "brunette", height: 1.65, weight: 60.6},
            {id: 3, name: "Thomas", gender: "male", dateOfBirth: "01.12.1920", hairColor: "ginger", height: 1.80, weight: 95.6},
            {id: 4, name: "Sarah", gender: "female", dateOfBirth: "01.12.1930", hairColor: "other", height: 2.10, weight: 92.0},
            {id: 5, name: "Jeff", gender: "male", dateOfBirth: "01.12.1940", hairColor: "brunette", height: 1.66, weight: 120.0},
        ]
    };

    render() {
        return (
            <>
                <h1>People</h1>
                <Table striped hover size="sm">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Date of birth</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.people.map((person: Person) =>
                        <PersonDataRow person={person} key={person.id}/>
                    )}
                    </tbody>
                </Table>
            </>
        );
    }
}

type PersonDataProps = { person: Person }
const PersonDataRow: FunctionComponent<PersonDataProps> = (props) => (
    <tr>
        <td>{props.person.name}</td>
        <td>{props.person.gender}</td>
        <td>{props.person.dateOfBirth}</td>
    </tr>
);
